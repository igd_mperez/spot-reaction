><> ><> ><> ><>
Docker development environment for NodeJS
><> ><> ><> ><>

Commands:
# Should be executed with root scope
sudo bash

# Build and run Docker image
docker-compose up --build

# Log into the Docker container
docker-compose exec <container> /bin/bash

# List active containers
docker ps

# Project files are under: /usr/src/app

# Apps are installed on Heroku / Update through each of the containers

><> ><> ><> ><>
Docker Installation
><> ><> ><> ><>

Doker
Reference: https://docs.docker.com/engine/installation/
Docker Compose
Reference: https://github.com/docker/compose/releases
