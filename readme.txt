# README #

Spotify integration using ReactJS

Main aspects:
- Created based on a Client / Server arquitecture
- Based the template: https://templated.co/synchronous
- Used Spotify's sample auth code for the Server
- Used a client Spotify Web API wrapper 
  (https://doxdox.org/jmperez/spotify-web-api-js)
- Used docker for the development environment (Client & Server)


####### ><> ><>

"Let this mind be in you which was also in Christ Jesus, who, 
being in the form of God, did not consider it robbery to be equal with God,
but made Himself of no reputation, taking the form of a bondservant, 
and coming in the likeness of men. And being found in appearance as a man,
He humbled Himself and became obedient to the point of death, 
even the death of the cross. 

Philippians 2:5-8

####### ><> ><>
