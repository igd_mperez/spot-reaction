import React, {Component} from 'react';

import SongList from './SongList';

class PlaylistDetail extends Component {
  render() {
    return (
      <div className="3u">
        <section id="sidebard2">
          <header>
            <h2>{this.props.playlist.name}</h2>
          </header>
          <SongList playlist={this.props.playlist} />
        </section>
      </div>
    );
  }
}

export default PlaylistDetail;
