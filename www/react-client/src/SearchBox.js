import React, {Component} from 'react';

import SearchBar from './SearchBar';
import SearchResultList from './SearchResultList';

class SearchBox extends Component {
  constructor(props) {
    super(props);
    
    this.initialState = {
      searchText: ''
    };
    
    this.state = this.initialState;
  }
  
  //Updates the search text
  handleSearch(event) {
    this.setState({
      searchText: event.target.value
    });
  }
  
  render() {
    return (
      <div className="SearchBox 9u skel-cell-important">
        <section id="box1">
          <SearchBar onChange={(event) => this.handleSearch(event)} />
          <SearchResultList 
            searchText={this.state.searchText}
            onAddToPlaylist={(id) => this.props.onAddToPlaylist(id)}
          />
        </section>
      </div>
    );
  }
}

export default SearchBox;
