import React, {Component} from 'react';

import LoggedBanner from './LoggedBanner';
import PlaylistBox from './PlaylistBox';

class SpotifyDashboard extends Component {
  constructor() {
    super();
    
    this.initialState = {
      currentPlaylist: {
        'name':'Not set',
        'id':0
      }
    };
    
    this.state = this.initialState;
  }
  
  //Method to set the selected playlist
  handlePlaylistChange(current) {
    this.setState({
      currentPlaylist:current}
    );
  }
  
  render() {
    return (
      <div className="SpotifyDashboard">
        <LoggedBanner 
          onPlaylistChange={(props) => this.handlePlaylistChange(props)}
        />
        <PlaylistBox playlist={this.state.currentPlaylist} />
      </div>
    );
  }
}

export default SpotifyDashboard;