import React, {Component} from 'react';

class SearchResult extends Component {
  render() {
    return (
      <li className={this.props.position === 0 ? 'SearchResult first' : 'SearchResult'} >
        <div>
          <span>{this.props.title}</span>
          <button onClick={() => this.props.onAddToPlaylist()} >Add to current playlist</button>
        </div>
      </li>
    );
  }
}

export default SearchResult;