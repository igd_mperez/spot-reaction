import React, {Component} from 'react';

class SearchBar extends Component {
  
  handleSubmit(event) {
    //Just to prevent the page reaload after form submitting
    event.preventDefault();
  }
  
  render() {
    return (
      <div className="SearchBar">
        <header>
          <form onSubmit={this.handleSubmit}>
            <input
              className="form-control"
              type="text"
              name="search"
              onChange={this.props.onChange}
              placeholder="Search for songs..." />
          </form>
        </header>
      </div>
    );
  }
}

export default SearchBar;