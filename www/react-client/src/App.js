import React, { Component } from 'react';
import SpotifyWebApi from 'spotify-web-api-js';

//Import custom components
import SpotifyDashboard from './SpotifyDashboard';
import Banner from './Banner';

//Spotify API
const spotifyApi = new SpotifyWebApi();

class App extends Component {
  constructor() {
    super();
    
    //Saves Spotify auth token
    //TODO: Refresh the auth token
    //mperez - JAN 16 2018
    const params = this.getHashParams();
    const token = params.access_token;
    if(token) {
      spotifyApi.setAccessToken(token);
    }

    this.state = {
      loggedIn: token ? true : false
    }
  }
  
  /**
  * Obtains parameters from the hash of the URL
  * @return Object
  */
  getHashParams() {
    var hashParams = {};
    var e, r = /([^&;=]+)=?([^&;]*)/g,
      q = window.location.hash.substring(1);
    e = r.exec(q)
    while (e) {
      hashParams[e[1]] = decodeURIComponent(e[2]);
      e = r.exec(q)
    }
    return hashParams;
  }
  
  render() {
  
    const isLoggedIn = this.state.loggedIn;
    let content;
    
    //Display the content based on the login status
    if(isLoggedIn) {
      content = <SpotifyDashboard />;
    } else {
      content = <Banner />;
    }
  
    return (
      <div className='App'>
        {content}
      </div>
    );
  }
}

export default App;
