import React, {Component} from 'react';
import SpotifyWebApi from 'spotify-web-api-js';

import PlaylistDetail from './PlaylistDetail';
import SearchBox from './SearchBox';

class PlaylistBox extends Component {
  
  //><> ><> ><>
  //Adds the song to the playlist
  handleAddtoPlaylist(songUri) {
    const spotifyApi = new SpotifyWebApi();
    
    spotifyApi.addTracksToPlaylist(this.props.playlist.id, [songUri])
      .then((response) => {
        //Updates the song list
      }, function(err) {
        alert('There was an error while adding the track to the playlist');
        console.log(err);
    });
  }
  
  render() {
    return (
      <div className="PlaylistBox" id="page">
        <div className="container">
          <div className="row">
            <PlaylistDetail playlist={this.props.playlist}/>
            <SearchBox 
              onAddToPlaylist={(id) => this.handleAddtoPlaylist(id)} />
          </div>
        </div>	
      </div>
    );
  }
}

export default PlaylistBox;