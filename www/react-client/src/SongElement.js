import React, {Component} from 'react';

class SongElement extends Component {
  render() {
    return (
      <li className={this.props.position === 0 ? 'SongElement first' : 'SongElement'} >
        <div>
          <span className="fa fa-check"></span>
          <a 
            href={this.props.songInfo.external_urls.spotify}
            target="_blank"
            rel="noopener noreferrer"
          > {this.props.songInfo.name}</a>
        </div>
      </li>
    );
  }
}

export default SongElement;