import React, {Component} from 'react';
import SpotifyWebApi from 'spotify-web-api-js';

import SearchResult from './SearchResult';

class SearchResultList extends Component {
  constructor(props) {
    super(props);
    
    this.initialState = {
      results: [],
      searchText: ''
    };
    
    this.state = this.initialState;
  }
  
  componentDidUpdate() {

    //><> ><> ><>
    //Gets the search results from Spotify
    if(this.props.searchText && this.state.searchText !== this.props.searchText) {
      
      //Update the search text in the local state
      this.setState({
        searchText: this.props.searchText
      });
      
      const spotifyApi = new SpotifyWebApi();
      
      spotifyApi.searchTracks(this.props.searchText)
        .then((response) => {
          this.setState({
            results: response.tracks.items
          });
        }, function (err) {
        this.setState({
          results: []
        });
      });
    }
    
  }
  
  render() {
    
    //Lists all the search results
    const results = this.state.results.map((element, index) => {
      return (
        <SearchResult
          key={element.id}
          position={index}
          title={element.name} 
          onAddToPlaylist={() => this.props.onAddToPlaylist(element.uri)}
        />
      ); 
    });
    
    return (
      <ul className="style3">
        {results}
      </ul>
    );
  }
}

export default SearchResultList;