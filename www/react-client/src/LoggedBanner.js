import React, {Component} from 'react';

import UserInfo from './UserInfo';
import PlaylistSelection from './PlaylistSelection';

class LoggedBanner extends Component {
  render() {
    return (
      <div id="header">
        <div className="container">
          <UserInfo />
          <PlaylistSelection 
            onPlaylistChange={(props) => this.props.onPlaylistChange(props)} 
          />
        </div>
      </div>
    );
  }
}

export default LoggedBanner;