import React, {Component} from 'react';
import SpotifyWebApi from 'spotify-web-api-js';

class PlaylistSelection extends Component {
  
  constructor(props) {
    super(props);
    
    this.initialState = {
      playlists: [],
      currentPlaylist: 0
    };
    
    this.state = this.initialState;
  }

  
  componentDidMount() {
    
    //><> ><> ><>
    //Gets the user information from Spotify
    
    //Use the Spotify API
    const spotifyApi = new SpotifyWebApi();
        
    spotifyApi.getUserPlaylists()
      .then((response) => {
        this.setState({
          playlists: response.items,
        });
        this.handlePlaylistChange(response.items[0]);
        
    }, function(err) {
      this.setState({
          playlists: []
        })
    });    
  }
  
  //Handles the event when the playlist is changed
  handlePlaylistChange(playlist) {
    this.setState({currentPlaylist: playlist});
    this.props.onPlaylistChange(playlist);
  }
  
  render() {
    //Displays all the playlists available
    const playlistSelection = this.state.playlists.map((playlist, index) => {
      return (
        <li 
          key={playlist.id}  
          className={playlist.id === this.state.currentPlaylist.id ? "active" : ""}
        >
          <a
            onClick={() => this.handlePlaylistChange({
              'name':playlist.name, 
              'id':playlist.id}
            )}
          >
            {playlist.name}
          </a>
        </li>
      )
    });
    
    return (
      <nav id="nav">
        <ul>{playlistSelection}</ul>
      </nav>
    );
  }
}

export default PlaylistSelection;