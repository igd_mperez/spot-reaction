import React, {Component} from 'react';
import SpotifyWebApi from 'spotify-web-api-js';

class UserInfo extends Component {
  constructor() {
    super();
    
    this.initialState = {
      name: 'Not set',
      followers: 0
    };
    
    this.state = this.initialState;
  }
  
  componentDidMount() {
    
    //><> ><> ><>
    //Gets the user information from Spotify
    
    //Use the Spotify API
    const spotifyApi = new SpotifyWebApi();
        
    spotifyApi.getMe()
      .then((response) => {
        this.setState({
          name: response.display_name,
          followers:response.followers.total
        })
    }, function(err) {
      this.setState({
          name: 'Expired Session',
          followers:0
        })
    });    
  }
  
  render() {
    return (
      <div id="logo">
        <h1><a href="/#">{this.state.name}</a></h1>
        <span>{this.state.followers} Followers</span>
      </div>
    );
  }
}

export default UserInfo;