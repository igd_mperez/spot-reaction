import React, {Component} from 'react';

class Banner extends Component {
  render() {
  
    //Url to the auth server
    const loginUrl = "http://spot-reaction.local:3000/login";
    
    return (
      <div id="header">
        <div className="container">
          <div id="logo">
            <h1><a href="/#">Spotify integration</a></h1>
            <span>Currently logged out</span>
          </div>
          <nav id="nav">
            <ul>
              <li className="active"><a href={loginUrl}> Login to Spotify </a></li>
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}

export default Banner;