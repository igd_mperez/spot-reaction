import React, {Component} from 'react';
import SpotifyWebApi from 'spotify-web-api-js';

import SongElement from './SongElement';

class SongList extends Component {
  
  state = {
    songs: []
  };
  
  componentDidUpdate() {
   
    //><> ><> ><>
    //Gets the song information from Spotify
    if(this.props.playlist) {
    
      const spotifyApi = new SpotifyWebApi();

      spotifyApi.getPlaylistTracks(this.props.playlist.id)
        .then((response) => {
          this.setState({
            songs: response.items
          });
          
      }, function(err) {

        this.setState({
          songs: [{
            'id':0,
            'name': 'No songs',
            'uri': ''
          }]
        });

      });
    }
    
  }

  render() {
    
    //Display the playlist song elements
    const songElements = this.state.songs.map((song, index) => {
      return (
        <SongElement
          key= {song.track.id}
          songInfo={song.track} 
          position={index}
        />
      );
    });
    
    return (
      <ul className="style1">
        {songElements}        
      </ul>
    );
  }
}

export default SongList;